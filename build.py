import os
import shutil

# Clean
shutil.rmtree("./builds", ignore_errors=True)
shutil.rmtree("./windows/dist", ignore_errors=True)
shutil.rmtree("./windows/build", ignore_errors=True)

# Create build directories
os.makedirs("./builds/windows-aio", exist_ok=True)
os.makedirs("./builds/windows", exist_ok=True)
os.makedirs("./builds/linux", exist_ok=True)
os.makedirs("./builds/windows-aio/db", exist_ok=True)

# Create windows exe
os.chdir("./windows")
os.system("pyinstaller --icon=icon.ico --onefile LSS.py")
os.chdir("..")

# Windows
shutil.copytree("./assets/common", "./builds/windows/", dirs_exist_ok=True)
shutil.copyfile("./assets/windows/app.exe","./builds/windows/app.exe")

# Windows all in one
shutil.copytree("./assets/common", "./builds/windows-aio/", dirs_exist_ok=True)
shutil.copytree("./assets/windows","./builds/windows-aio/", dirs_exist_ok=True)
shutil.copyfile("./windows/dist/LSS.exe","./builds/windows-aio/LSS.exe")
shutil.copyfile("./windows/db/mongod.exe", "./builds/windows-aio/db/mongod.exe")

# Zip
shutil.make_archive("./builds/windows", "zip", "./builds/windows")
shutil.make_archive("./builds/windows-aio", "zip", "./builds/windows-aio")
