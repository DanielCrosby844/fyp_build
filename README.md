# Final Year Project - Build

Simple python script to build the final application.

## Other Repositories

- UI: https://bitbucket.org/DanielCrosby844/fyp_ui
- API: https://bitbucket.org/DanielCrosby844/fyp_api

## Dependancies

```console
pip install pyinstaller
```

## Setup

1. Create an assets folder
2. Create subfolders common/windows
3. Place required files in correct directories
    - common: config.json, app(UI build)
    - windows: app.exe(API build), jre 11

## Run
```console
python build.py
```
