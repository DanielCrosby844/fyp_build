import os
import subprocess
from datetime import datetime

if __name__ == '__main__':
    os.makedirs("./logs", exist_ok=True)
    date = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    if(os.path.isfile("./db/mongod.exe")):
        os.makedirs("./db/data", exist_ok=True)
        subprocess.Popen(f"start /b \"MongoDB\" ./db/mongod.exe --dbpath ./db/data > ./logs/mongo-log-{date}.txt", close_fds=True, shell=True)
    else:
        print("Warning no mongo db exe found.")
    try:
        subprocess.Popen(".\\jre\\bin\\java.exe -jar app.exe", close_fds=True, shell=True)
    except:
        print("System exiting")
